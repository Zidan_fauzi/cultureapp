<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){

	    parent::__construct();

	    $this->load->model('M_artikel');
        $this->load->model('M_user');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        if ($this->session->userdata('id_admin') == null) {
            redirect('Login');
        }
	}
	public function index()
	{
		$data['artikel'] = $this->M_artikel->getAll();
		$data['user'] = $this->M_user->getAll();
		
		$data['page'] = 'Dashboard';
		$data['css'] = '
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css">
			<link href="'.base_url().'/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
			<link href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
			<link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
		';
		$data['js'] = '
			<script src="'.base_url().'/assets/global/plugins/respond.min.js"></script>
			<script src="'.base_url().'/assets/global/plugins/excanvas.min.js"></script> 
			<script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/admin/pages/scripts/index3.js" type="text/javascript"></script>
			<script src="'.base_url().'/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
			<script>
				jQuery(document).ready(function() {    
				   	Metronic.init(); // init metronic core componets
				   	Layout.init(); // init layout
				   	Demo.init(); // init demo features 
					Index.init(); // init index page
					Tasks.initDashboardWidget(); // init tash dashboard widget  
				});
			</script>';
		$this->load->view('manager/header', $data);
		$this->load->view('manager/dashboard', $data);
		$this->load->view('manager/footer');
	}

	public function Encode()
	{
		$image = file_get_contents(base_url().'file/kawaiii_maho.png');
		$dat = base64_encode($image);
		var_dump($dat);
	}
}
