<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_Berita extends CI_Controller {

        function __construct(){

                parent::__construct();
    
                // error_reporting(0);
                $this->load->model('M_artikel');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
                if ($this->session->userdata('id_admin') == null) {
                    redirect('Login');
                }
        }

        public function index()
        {
            $data['Subab'] = $this->M_artikel->getAllSubab();        

            $data['page'] = 'Artikel';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/ui-toastr.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    UIToastr.init();
                                        
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/manage_artikel', $data);
            $this->load->view('manager/footer');
        }

        public function indexSubab()
        {
            $data['artikel'] = $this->M_artikel->getAll();           

            $data['page'] = 'Artikel';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/ckeditor/ckeditor.js"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/ui-toastr.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    UIToastr.init();
                                        
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/insert_subab', $data);
            $this->load->view('manager/footer');
        }

        public function indexEditSubab($id)
        {
            $data['Subab'] = $this->M_artikel->getAllSubabById($id); 
            $data['artikel'] = $this->M_artikel->getAllBySubab($id);        

            $data['page'] = 'Artikel';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/ckeditor/ckeditor.js"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/ui-toastr.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    UIToastr.init();
                                        
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/update_subab', $data);
            $this->load->view('manager/footer');
        }

        public function indexCreate($id)
        {
            $data['page'] = 'Artikel';
            $data['Subab'] = $this->M_artikel->getAllSubabById($id);
            $data['css'] = '
               <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
               <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
               <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/ckeditor/ckeditor.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>

                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/form-samples.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/components-editors.js"></script>
                <script>
                    jQuery(document).ready(function() {    
                        // initiate layout and plugins
                        Metronic.init(); // init metronic core components
                        Layout.init(); // init current layout
                        Demo.init(); // init demo features
                        FormSamples.init();
                        ComponentsEditors.init();
                    });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/insert_artikel', $data);
            $this->load->view('manager/footer'); 
        }

        public function indexEdit($id)
        {
            $data['page'] = 'Artikel';

            $data['css'] = '
               <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
               
               <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
               <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
               <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/ckeditor/ckeditor.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>

                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/form-samples.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/components-editors.js"></script>
                <script>
                    jQuery(document).ready(function() {    
                        // initiate layout and plugins
                        Metronic.init(); // init metronic core components
                        Layout.init(); // init current layout
                        Demo.init(); // init demo features
                        FormSamples.init();
                        ComponentsEditors.init();
                    });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
                
            ';

            $data["article"] = $this->M_artikel->getById($id);
            // var_dump($this->db->last_query());
            // var_dump($data["article"]);exit();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/update_artikel', $data);
            $this->load->view('manager/footer'); 
        }

        public function indexPageDetail()
        {
            $response = $this->M_artikel->getPageDetail($_POST['id']);
            echo json_encode($response);
        }

        public function Create($id)
        {
            $title = $this->input->post('title');
            $desc = $this->input->post('descArtikel');
            $total_page = (int)$this->input->post('TotalPage');
            // Begin Upload Article
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
            $config['upload_path'] = './file/';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name_cover = 'file/'.$upload_data['file_name'];                
            }
            // Set Data Article
            $data_article = array(
                'article_description' => $desc,
                'article_title' => $title,
                'article_image' => $file_name_cover,
                'created_at' => date("y-m-d H:i:s"),
                'subab_id' => $id
            );
            
            $this->M_artikel->uploadArticle($data_article); 

            $idArticle = $this->db->insert_id();
            // Set Data Article Page
            $data_page = array(
                'article_id' => $this->db->insert_id(),
                'page_number' => $total_page
            );

            $sendData_Artikel = $this->M_artikel->uploadPage($data_page);
            $id = $this->db->insert_id();
            
            if ($sendData_Artikel) {
                for ($i = 1; $i <= $total_page; $i++) { 
                    $data_detail_page = array(
                        'page_detail_type' => $_POST['detail_type'.$i],
                        'page_detail_content' => $_POST['page_content'.$i],
                        'page_detail_order'=> $i,
                        'page_id' => $id
                    );
                    $this->M_artikel->uploadPageDetail($data_detail_page);
                }
            }

            $data["article"] = $this->M_artikel->getById($idArticle);
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('method', 'Create');
            $this->session->set_flashdata('ArticleName', $data["article"]['article_title']);
            redirect(site_url('Manage_Berita/indexEditSubab/'.$data["article"]['subab_id']));
        }

        public function edit($id)
        {
            $title = $this->input->post('title');
            $desc = $this->input->post('descArtikel');

            $total_page = (int)$this->input->post('TotalPage');
            // Begin Upload Article
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
            $config['upload_path'] = './file/';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name_cover = 'file/'.$upload_data['file_name'];

                $data_article = array(
                    'article_description' => $desc,
                    'article_image' => $file_name_cover,
                    'article_title' => $title
                );
                
                $this->M_artikel->updateArticle($id, $data_article); 

                // Set Data Article Page
                $data_page = array(
                    'article_id' => $id,
                    'page_number' => $total_page
                );

                $data["article"] = $this->M_artikel->getById($id);
                $sendData_Artikel = $this->M_artikel->updatePage($data["article"]['page_id'], $data_page);
                
                $deletePage_detail = $this->M_artikel->deletePageDetail($data["article"]['page_id']);
                for ($i = 0; $i <= $total_page-1; $i++) { 
                    $data_detail_page = array(
                        'page_detail_type' => $_POST['detail_type'],
                        'page_detail_content' => $_POST['page_content'.$i],
                        'page_detail_order'=> $i + 1,
                        'page_id' => $data["article"]['page_id']
                    );
                    $this->M_artikel->uploadPageDetail($data_detail_page);
                }        
            }else{
                // Set Data Article
                $data_article = array(
                    'article_description' => $desc,
                    'article_title' => $title
                );
                
                $this->M_artikel->updateArticle($id, $data_article); 
                // Set Data Article Page
                $data_page = array(
                    'article_id' => $id,
                    'page_number' => $total_page
                );

                $data["article"] = $this->M_artikel->getById($id);
                $sendData_Artikel = $this->M_artikel->updatePage($data["article"]['page_id'], $data_page);
                
                $this->M_artikel->deletePageDetail($data["article"]['page_id']);
                for ($i = 0; $i <= $total_page-1; $i++) { 
                    $data_detail_page = array(
                        'page_detail_type' => $_POST['detail_type'],
                        'page_detail_content' => $_POST['page_content'.$i],
                        'page_detail_order'=> $i + 1,
                        'page_id' => $data["article"]['page_id']
                    );
                    $this->M_artikel->uploadPageDetail($data_detail_page);
                }
            }

            $data["article"] = $this->M_artikel->getById($id);
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('method', 'Update');
            $this->session->set_flashdata('ArticleName', $data["article"]['article_title']);
            redirect(site_url('Manage_Berita/indexEditSubab/'.$data["article"]['subab_id']));
        }

        public function delete($id)
        {
            $data["artikel"]= $this->M_artikel->getById($id);
            if (unlink($data["artikel"]["article_image"])) {
                $articleName = $data["artikel"]['article_title'];
                $id_subab = $data["artikel"]['subab_id'];
                $this->M_artikel->deletePageDetail($data["article"]['page_id']);
                $this->M_artikel->delete($id);

                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('method', 'Delete');
                $this->session->set_flashdata('ArticleName', $articleName);
                redirect(site_url('Manage_Berita/indexEditSubab/'.$id_subab));
            }
        }

        public function CreateSubab()
        {
            $title = $this->input->post('titleSubab');
            $description = $this->input->post('detailSubab');
            $category = $this->input->post('category');
            // Begin Upload Article
            $nmfile = "Subab_image_".time();
            $config['file_name'] = $nmfile;
            $config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
            $config['upload_path'] = './file/';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name_cover = 'file/'.$upload_data['file_name'];                
            }
            // Set Data Article
            $data_article = array(
                'subab_title' => $title,
                'subab_image' => $file_name_cover,
                'category_id' => $category,
                'created_at' => date("y-m-d H:i:s"),
                'subab_description' => $description
            );
            
            $this->M_artikel->uploadSubab($data_article); 

            $this->session->set_flashdata('status', 'Success');
            $this->session->set_flashdata('method', 'Create');
            $this->session->set_flashdata('ArticleName', $title);
            redirect('Manage_Berita');
        }

        public function editSubab($id)
        {
            $title = $this->input->post('titleSubab');
            $description = $this->input->post('detailSubab');
            $category = $this->input->post('category');
            // Begin Upload Article
            $nmfile = "Subab_image_".time();
            $config['file_name'] = $nmfile;
            $config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
            $config['upload_path'] = './file/';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name_cover = 'file/'.$upload_data['file_name'];
                // var_dump($file_name_cover);exit();
                $data_subab = array(
                    'subab_title' => $title,
                    'subab_image' => $file_name_cover,
                    'category_id' => $category,
                    'created_at' => date("y-m-d H:i:s"),
                    'subab_description' => $description
                );
                
                $this->M_artikel->updateSubab($data_subab, $id);      
            }else{
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name_cover = 'file/'.$upload_data['file_name'];

                $data_subab = array(
                    'subab_title' => $title,
                    'category_id' => $category,
                    'created_at' => date("y-m-d H:i:s"),
                    'subab_description' => $description
                );
                
                $this->M_artikel->updateSubab($data_subab, $id); 
            }

            $data["article"] = $this->M_artikel->getAllSubabById($id);
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('method', 'Update');
            $this->session->set_flashdata('ArticleName', $data["article"]['subab_title']);
            redirect(site_url('Manage_Berita'));
        }
       
        public function deleteSubab($id)
        {
            $data["artikel"] = $this->M_artikel->getAllBySubab($id);
            $data["subab"] = $this->M_artikel->getAllSubabById($id);
            $id_subab = $data["subab"]['subab_id'];
            $articleName = $data["subab"]['subab_title'];

            if (count($data["artikel"]) > 0) {
                for ($i=0; $i < count($data["artikel"]); $i++) { 
                    if (unlink($data["artikel"][$i]["article_image"])) {

                        $this->M_artikel->deletePageDetail($data["artikel"][$i]['page_id']);
                        $this->M_artikel->deletePage($data["artikel"][$i]['page_id']);
                        $this->M_artikel->delete($data["artikel"][$i]['article_id']);
                    }
                }

                if (unlink($data["subab"]["subab_image"])) {
                    $this->M_artikel->deleteSubab($id_subab);
                }

                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('method', 'Delete');
                $this->session->set_flashdata('ArticleName', $articleName);
                redirect(site_url('Manage_Berita/'));
            }else{
                if (unlink($data["subab"]["subab_image"])) {
                    $this->M_artikel->deleteSubab($id_subab);
                }

                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('method', 'Delete');
                $this->session->set_flashdata('ArticleName', $articleName);
                redirect(site_url('Manage_Berita/'));
            }
        }
    }
?>