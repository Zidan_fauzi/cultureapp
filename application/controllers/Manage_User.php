<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_User extends CI_Controller {

        function __construct(){

                parent::__construct();
    
                // error_reporting(0);
                $this->load->model('M_user');
                $this->load->model('M_login');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
                if ($this->session->userdata('id_admin') == null) {
                    redirect('Login');
                }
        }

        public function index()
        {
            $data['user'] = $this->M_user->getAll();             

            $data['page'] = 'User';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/ui-toastr.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    UIToastr.init();
                                        
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/user/manage_user', $data);
            $this->load->view('manager/footer');
        }

        public function indexCreate()
        {  
            $data['page'] = 'User';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/ui-toastr.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    UIToastr.init();
                                        
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/user/insert_user', $data);
            $this->load->view('manager/footer'); 
        }       

        public function Create()
        {
            $pengacak = "p3ng4c4k";
            $password = $_POST['password'];
            $encrypt1 = MD5($password);
            $encrypt2 = md5($pengacak . md5($encrypt1));
            $imageName = 'ImageProfile_'.time().'.png';

            $config['file_name'] = $imageName;
            $config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
            $config['upload_path'] = './file/';
            $this->load->library('upload', $config);

            $response = $this->M_login->checkUser($_POST['email']);

            if (count($response) > 0) {
                $this->session->set_flashdata('status', 'failed');
                $this->session->set_flashdata('method', 'Create');
                $this->session->set_flashdata('email', $_POST['email']);
                $this->session->set_flashdata('nama', $_POST['username']);
                $this->session->set_flashdata('password', $password);
                redirect(site_url('Manage_User/indexCreate'));
            }else{
                if($this->upload->do_upload('gambar')){
                    $post = $this->input->post();
                    $upload_data = $this->upload->data();
                    $file_name_cover = 'file/'.$upload_data['file_name']; 
                    $dataUpload = array(
                        'gambar' => $file_name_cover,
                        'nama' => $_POST['username'],
                        'email' => $_POST['email'],
                        'password' => $encrypt2,
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                        'level' => '1',
                        'is_delete' => '0',
                    );
                    $this->M_login->register($dataUpload);
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('method', 'Create');
                    $this->session->set_flashdata('email', $email);               
                }else{
                    $dataUpload = array(
                        'nama' => $_POST['username'],
                        'email' => $_POST['email'],
                        'password' => $encrypt2,
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s'),
                        'level' => '1',
                        'is_delete' => '0',
                    );
                    $this->M_login->register($dataUpload);
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('method', 'Create');
                    $this->session->set_flashdata('email', $email);
                }
                
                redirect(site_url('Manage_User'));
                exit();
            }
            
        }   

         public function delete($id)
        {
            $data["user"]= $this->M_user->getById($id);
            if (unlink($data["user"]["gambar"])) {
                $articleName = $data["user"]['username'];
                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('method', 'Delete');
                $this->session->set_flashdata('ArticleName', $articleName);
                $this->M_user->delete($id);
                redirect(site_url('Manage_User'));
            }
        }
    }
?>