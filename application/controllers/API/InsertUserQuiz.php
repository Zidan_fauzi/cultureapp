<?php
class InsertUserQuiz extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_login');
    }

    public function index()
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json,true);

        $pengacak = "p3ng4c4k";
        $User_ID = $obj['user_id'];        
        $Quiz_ID = $obj['quiz_id'];

        $data = array(
            'id_admin' => $User_ID,
            'quiz_id' => $Quiz_ID
        );

        $kirim = $this->db->insert('user_quiz', $data);   
        if ($kirim) {
            $session_data[] = array(
                'status' => 'success',
                'response' => '200'
            );
        }else{
            $session_data[] = array(
                'msg' => 'Question Not Found',
                'status' => 'Bad Request',
                'response' => '400'
            );
        }
        echo json_encode($session_data);
    }    
}
?>