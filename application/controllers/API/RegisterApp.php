<?php
class RegisterApp extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_login');
    }

    public function index()
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json,true);

        $pengacak = "p3ng4c4k";
        $password = $obj['password'];
        $encrypt1 = MD5($password);
        $encrypt2 = md5($pengacak . md5($encrypt1));

        $data = $obj['image'];        
        if ($data != "null") {
            $data = base64_decode($data);
            $imageName = 'ImageProfile_'.time().'.png';
            $gambar = 'file/'.$imageName;

            $dataUpload = array(
                'gambar' => $gambar,
                'nama' => $obj['username'],
                'email' => $obj['email'],
                'password' => $encrypt2,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
                'level' => '1',
                'is_delete' => '0',
            );
            $response = $this->M_login->checkUser($obj['email']);

            $this->registerWithImage($dataUpload, $response, $imageName, $data);
        }else{
            $dataUpload = array(
                'nama' => $obj['username'],
                'email' => $obj['email'],
                'password' => $encrypt2,
                'created_at' => date('y-m-d H:i:s'),
                'updated_at' => date('y-m-d H:i:s'),
                'level' => '1',
                'is_delete' => '0',
            );
            $response = $this->M_login->checkUser($obj['email']);

            $this->registerWithoutImage($dataUpload, $response);
        }

    }

    public function registerWithImage($data, $response, $imageName, $dataImage)
    {
        if (count($response) > 0) {
            $session_data[] = array(
                'massage' => 'Sorry this email is already used by another user',
                'status' => 'Bad Request',
                'response' => '400'
            );
            echo json_encode($session_data);
        }else{
            if (file_put_contents('file/'.$imageName, $dataImage)) {
                $responseRegis = $this->M_login->register($data);
                $session_data[] = array(
                    'massage' => 'Registration Success',
                    'status' => 'Success',
                    'response' => '200'
                );
                echo json_encode($session_data);
            }else{
                $session_data[] = array(
                    'massage' => 'Registration Failed',
                    'status' => 'Bad Request',
                    'response' => '400'
                );
                echo json_encode($session_data);
            }
        }
    }

    public function registerWithoutImage($data, $response)
    {
        if (count($response) > 0) {
            $session_data[] = array(
                'massage' => 'Sorry this email is already used by another user',
                'status' => 'Bad Request',
                'response' => '400'
            );
            echo json_encode($session_data);
        }else{
            $responseRegis = $this->M_login->register($data);
            $session_data[] = array(
                'massage' => 'Registration Success',
                'status' => 'Success',
                'response' => '200'
            );
            echo json_encode($session_data);
        }
    }
}
?>