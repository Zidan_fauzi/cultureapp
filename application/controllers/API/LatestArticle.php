<?php
class LatestArticle extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_artikel');
    }

    public function index()
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json,true);
        $session_data = array();
        $artikelSubab = $this->M_artikel->getAllSubab();
        if (count($artikelSubab) <= 0) {
            $session_data[] = array(
                'status' => 'Not Found',
                'response' => '404'
            );
        }else{
            $dataSubab = array();
            $dataArtikel = array();
            for ($i=0; $i < count($artikelSubab) ; $i++) { 
                $artikel = $this->M_artikel->getAllBySubab($artikelSubab[$i]['subab_id']);
                for ($j=0; $j < count($artikel) ; $j++) {
                    $dataArtikel[$j] = array(  
                        'subab_id' => $artikel[$j]['article_id'],
                        'subab_title' => $artikel[$j]['article_title'],
                        'subab_desc' => $artikel[$j]['article_description'],
                        'subab_image' => $artikel[$j]['article_image'],
                        'subab_created_at' => $artikel[$j]['created_at']
                    );
                }
                $dataSubab[$i] = array(
                    'article_id' => $artikelSubab[$i]['subab_id'],
                    'article_title' => $artikelSubab[$i]['subab_title'],
                    'category_id' => $artikelSubab[$i]['category_id'],
                    'category_name' => $artikelSubab[$i]['category_name'],
                    'article_image' => $artikelSubab[$i]['subab_image'],
                    'article_desc' => $artikelSubab[$i]['subab_description'],
                    'article_created_at' => $artikelSubab[$i]['created_at'],
                    'Subab' => $dataArtikel
                );
            }
            $session_data[] = array(
                'data' => $dataSubab,
                'status' => 'success',
                'response' => '200'
            );
        }

        echo json_encode($session_data);
    }    
}
?>