<?php
class LoginApp extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_login');
    }

    public function index()
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json,true);

        $pengacak = "p3ng4c4k";
        $email = $obj['email'];
        $password = MD5($obj['password']);
        $password = md5($pengacak . md5($password));
        
        $checking = $this->M_login->check_login('tb_admin', array('email' => $email), array('password' => $password), array('updated_at' => date("y-m-d H:i:s")) );
        if ($checking != FALSE) {
            foreach ($checking as $apps) {                
                $this->login($apps, true);                
            }
        }else{
            $apps = 'Not Found';
            $this->login($apps, FALSE);
        }
    }
    public function login($data, $status)
    {
        if ($status) {
            $session_data[] = array(                          
                'data' =>array(
                    'id_admin' => $data->id_admin,
                    'email' => $data->email,
                    'nama' => $data->nama,
                    'created_at' => $data->created_at,
                    'gambar' => $data->gambar,
                    'password' => $data->password,                          
                    'level'   => $data->level,
                ),
                'status' => 'success',
                'response' => '200'
            );
        }else{
            $session_data[] = array(
                'status' => 'not found',
                'response' => '400'
            );
        }

        echo json_encode($session_data);
    }
}
?>