<?php
class SubabByID extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_artikel');
        $this->load->model('M_quiz');
    }

    public function index()
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json,true);
        
        $article_id = $obj['article_id'];

        $Artikel = $this->M_artikel->getAllSubabById($article_id);
        if ($Artikel == false) {
            $session_data[] = array(
                'status' => 'Not Found',
                'response' => '404'
            );
        }else{
            $datasubab = array();
            $quizAnswer = array();
            $Question = array();

            $Subab = $this->M_artikel->getAllBySubab($Artikel['subab_id']);
            for ($h=0; $h < count($Subab); $h++) { 
                $pageSubab = $this->M_artikel->getPageDetail($Subab[$h]['page_id']);
                $Quiz = $this->M_quiz->getQuizById($Subab[$h]['article_id']);
                for ($i=0; $i < count($Quiz) ; $i++) { 
                    $QuizAnswer = $this->M_quiz->getQuizAnswer($Quiz[$i]["quiz_id"]);
                    for ($j=0; $j < count($QuizAnswer); $j++) { 
                       $quizAnswer[$j] = array(
                        'quiz_answer_content' => $QuizAnswer[$j]["quiz_answer_content"],
                        'quiz_answer_key' => $QuizAnswer[$j]["quiz_answer_key"]
                       );
                    }
                    $Question[] = array(
                        'number' => $i+1,
                        'quiz_content' => $Quiz[$i]["quiz_content"],
                        'quiz_answer_key' => $Quiz[$i]["quiz_answer_key"],
                        'Answer' => $quizAnswer
                    );
                }
                $datasubab[] = array(                          
                    'subab_id' => $Subab[$h]['article_id'],
                    'subab_title' => $Subab[$h]['article_title'],
                    'subab_desc' => $Subab[$h]['article_description'],
                    'subab_image' => $Subab[$h]['article_image'],
                    'created_at' => $Subab[$h]['created_at'],
                    'page_subab' => $pageSubab
                );
            }           
            $session_data[] = array(                          
                'data' =>array(
                    'article_id' => $Artikel['subab_id'],
                    'article_title' => $Artikel['subab_title'],
                    'article_desc' => $Artikel['subab_description'],
                    'article_image' => $Artikel['subab_image'],
                    'category_id' => $Artikel['category_id'],
                    'category_name' => $Artikel['category_name'],
                    'Subab' => $datasubab,
                    'question' => $Question,
                    'status' => 'success',
                    'response' => '200'
                )
            );
        }

        echo json_encode($session_data);
    }    
}
?>