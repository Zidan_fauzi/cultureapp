<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_Quiz extends CI_Controller {

        function __construct(){

                parent::__construct();
    
                // error_reporting(0);
                $this->load->model('M_artikel');
                $this->load->model('M_quiz');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
                if ($this->session->userdata('id_admin') == null) {
                    redirect('Login');
                }
        }

        public function index()
        {
            $data['article'] = array();
            $data['artikel'] = $this->M_artikel->getAll(); 
            $w = count($data['artikel']);

            for ($h=0; $h < $w; $h++) { 
                $data["articleByID"] = $this->M_artikel->getById($data['artikel'][$h]['article_id']);
                $data['quiz'] = $this->M_quiz->getQuiz($data['articleByID']['article_id']);
                $y = count($data['quiz']);

                $data['article'][$h] = array(
                    'number' => $h,
                    'article_id' => $data["articleByID"]['article_id'],
                    'TotalAnswerPerQuiz' => $data["articleByID"]["quiz_total_answer"],
                    'article_title' => $data["articleByID"]['article_title'],
                    'created_at' => $data["articleByID"]['created_at'],
                    'TotalQuiz' => $y
                );
            }
            // var_dump($data['article']);exit();

            $data['page'] = 'Quiz';
            $data['css'] = '
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
                <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
                <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
                <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/table-managed.js"></script>
                <script>
                jQuery(document).ready(function() {       
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features
                    TableManaged.init();
                    $("#draggable").draggable({
                        handle: ".modal-header"
                    });
                });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/quiz/manage_quiz', $data);
            $this->load->view('manager/footer');
        }

        public function IndexQuiz()
        {
            $responer = array();
            $quizAnswer = array();
            $Quiz = $this->M_quiz->getQuizById($_POST['id']);
            $x = count($Quiz);
            for ($i=0; $i < $x ; $i++) { 
                $QuizAnswer = $this->M_quiz->getQuizAnswer($Quiz[$i]["quiz_id"]);
                $y = count($QuizAnswer);
                for ($j=0; $j < $y ; $j++) { 
                   $quizAnswer[$j] = array(
                    'number' => $j,
                    'quiz_answer_content' => $QuizAnswer[$j]["quiz_answer_content"],
                    'quiz_answer_key' => $QuizAnswer[$j]["quiz_answer_key"]
                   );
                }
                $responer[$i] = array(
                    'number' => $i,
                    'quiz_content' => $Quiz[$i]["quiz_content"],
                    'quiz_answer_key' => $Quiz[$i]["quiz_answer_key"],
                    'quizAnswer' => $quizAnswer
                );
            }
            echo json_encode($responer);
        }

        public function indexCreate()
        {
            $data['artikel'] = $this->M_artikel->getAll();
            $data['page'] = 'Quiz';
            $data['css'] = '
               <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
               // <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
               <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
               <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/fuelux/js/spinner.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/ckeditor/ckeditor.js"></script>
                
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/components-form-tools.js"></script>
                
                <script>
                    jQuery(document).ready(function() {       
                        // initiate layout and plugins
                        Metronic.init(); // init metronic core components
                        Layout.init(); // init current layout
                        Demo.init(); // init demo features
                        ComponentsFormTools.init();
                    });   
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
            ';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/quiz/insert_quiz', $data);
            $this->load->view('manager/footer'); 
        }

        public function indexEdit($id)
        {
            $data['page'] = 'Quiz';
            $data['css'] = '
               <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
               <link href="'.base_url().'/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.css">
               <link rel="stylesheet" type="text/css" href="'.base_url().'/assets/global/plugins/select2/select2.css"/>
               <link href="'.base_url().'/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
               <link id="style_color" href="'.base_url().'/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
               <link href="'.base_url().'/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
            ';
            $data['js'] = '
                <script src="'.base_url().'/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>

                <script src="'.base_url().'/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="'.base_url().'/assets/global/plugins/select2/select2.min.js"></script>
                <script src="'.base_url().'/assets/global/scripts/metronic.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/form-samples.js"></script>
                <script src="'.base_url().'/assets/admin/pages/scripts/components-editors.js"></script>
                <script>
                    jQuery(document).ready(function() {    
                        // initiate layout and plugins
                        Layout.init(); // init current layout
                        Demo.init(); // init demo features
                        FormSamples.init();
                        ComponentsEditors.init();
                    });
                </script>
                <script type="text/javascript" src="'.base_url().'/assets/plugin/dropify/dropify.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".dropify").dropify({
                            messages: {
                                default: "Drag or drop for choose image",
                                replace: "Change",
                                remove:  "Delete",
                                error:   "Error"
                            }
                        });
                    });
                </script>
                
            ';
            $data['artikel'] = $this->M_artikel->getAll();
            $data['quiz'] = $this->M_quiz->getQuizByArticle($id);

            $this->load->view('manager/header', $data);
            $this->load->view('manager/quiz/update_quiz', $data);
            $this->load->view('manager/footer'); 
        }

        public function uploadAnswer($id)
        {
            for ($i=1; $i <= $this->input->post('banyakJawaban'); $i++) { 
                $AnswerQuestion = $this->input->post('answerQuestion'.$i);
                $data_question_answer = array(
                    'quiz_answer_content' => $AnswerQuestion, 
                    'quiz_id' => $id,
                    'quiz_answer_key' =>$i
                );
                // Model
                $this->M_quiz->uploadQuizAnswer($data_question_answer);
            }
            return true;
        }

        public function updateAnswer($id)
        {
            for ($i=0; $i < $this->input->post('banyakJawaban'); $i++) { 
                $dataID = $i +1;
                $AnswerQuestion = $this->input->post('answerQuestion'.$dataID);
                $data_question_answer = array(
                    'quiz_answer_content' => $AnswerQuestion, 
                    'quiz_id' => $id,
                    'quiz_answer_key' =>$i + 1
                );
                var_dump($data_question_answer);
                // Model
                $this->M_quiz->uploadQuizAnswer($data_question_answer);
            }
            return true;
        }

        public function uploadAnswerKey($id, $i)
        {
            $AnswerKey = $this->input->post('AnswerKey'.$i);
            $data_question_answer_key = array(
                'quiz_id' => $id,
                'quiz_answer_key' => $AnswerKey
            );
            $this->M_quiz->uploadQuizAnswerKey($data_question_answer_key);
        }

        public function updateAnswerKey($id, $i)
        {
            $AnswerKey = $this->input->post('AnswerKey'.$i);
            $data_question_answer_key = array(
                'quiz_id' => $id,
                'quiz_answer_key' => $AnswerKey
            );
            $this->M_quiz->uploadQuizAnswerKey($data_question_answer_key);
        }

        public function Create()
        {
            $total_question = (int)$this->input->post('banyakSoal');
            for ($i=1; $i <= $total_question; $i++) { 
                $question = $this->input->post('quiz_question'.$i);
                $articleId = $this->input->post('articleId');
                $data = array(
                    'article_id' => $articleId,
                    'quiz_total_answer' => $this->input->post('banyakJawaban'),
                    'quiz_content' => $question
                );
                // Model
                $this->M_quiz->uploadQuiz($data);
                $id = $this->db->insert_id();
                $InputAnswer = $this->uploadAnswer($id);
                if ($InputAnswer) {
                    $this->uploadAnswerKey($id, $i);
                }else{
                    echo "gagal";
                    exit();
                }
            }
            redirect(site_url('Manage_Quiz'));
        }    

        public function Edit($id)
        {
            $total_question = (int)$this->input->post('banyakSoal');
            $data['Quiz'] = $this->M_quiz->getQuizById($id);
                        
            for ($i=0; $i < count($data['Quiz']); $i++) { 
                $this->M_quiz->deleteQuiz($id);
                $this->M_quiz->deleteAnswer($data['Quiz'][$i]['quiz_id']);
                $this->M_quiz->deleteAnswerKey($data['Quiz'][$i]['quiz_id']);
            }

            for ($i=0; $i < $total_question; $i++) { 
                $question = $this->input->post('quiz_question'.$i);
                $articleId = $this->input->post('articleId');
                $data = array(
                    'article_id' => $articleId,
                    'quiz_total_answer' => $this->input->post('banyakJawaban'),
                    'quiz_content' => $question
                );
                // Model
                $this->M_quiz->uploadQuiz($data);
                $idQuiz = $this->db->insert_id();
                $InputAnswer = $this->updateAnswer($idQuiz);
                if ($InputAnswer) {
                    $this->updateAnswerKey($idQuiz, $i);
                }else{
                    echo "gagal";
                    exit();
                }
            }
            redirect(site_url('Manage_Quiz'));
        }

        public function delete($id)
        {
            $data['Quiz'] = $this->M_quiz->getQuizById($id);
            for ($i=0; $i < count($data['Quiz']); $i++) { 
                $this->M_quiz->deleteQuiz($id);
                $this->M_quiz->deleteAnswer($data['Quiz'][$i]['quiz_id']);
                $this->M_quiz->deleteAnswerKey($data['Quiz'][$i]['quiz_id']);
            }

            redirect(site_url('Manage_Quiz'));
        }
    }
?>