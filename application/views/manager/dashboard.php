<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Dashboard <small>statistics & reports</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb hide">
        <li>
          <a href="javascript:;">Home</a><i class="fa fa-circle"></i>
        </li>
        <li class="active">
           Dashboard
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- BEGIN PAGE CONTENT INNER -->
      <div class="row margin-top-10">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="dashboard-stat2" style="padding: 1px 15px; margin: 0px auto 25px auto; ">
            <div class="display">
              <div class="number">
                <h2 class="font-green-sharp"><?php echo count($artikel); ?></h2>
                <small>TOTAL ARTICLE</small>
              </div>
              <div class="icon" style="margin-top: 35px;">
                <i class="icon-notebook" style="font-size: 46px;"></i>
              </div>
            </div>        
          </div>
        </div>        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="dashboard-stat2" style="padding: 1px 15px; margin: 0px auto 25px auto; ">
            <div class="display">
              <div class="number">
                <h2 class="font-purple-soft"><?php echo count($user); ?></h2>
                <small>TOTAL USER</small>
              </div>
              <div class="icon" style="margin-top: 35px;">
                <i class="icon-user" style="font-size: 46px;"></i>
              </div>
            </div>        
          </div>
        </div>    
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <!-- BEGIN PORTLET-->
          <div class="portlet light">
            <div class="portlet-title tabbable-line">
              <div class="caption caption-md">
                <i class="icon-globe theme-font-color hide"></i>
                <span class="caption-subject theme-font-color bold uppercase">Member Activity</span>
              </div>
            </div>
            <div class="portlet-body">
              <!--BEGIN TABS-->
              <div class="tab-content">
                <div class="tab-pane active">
                  <div class="scroller" style="height: 337px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                    <table class="table table-hover table-light">
                      <thead>
                      <tr class="uppercase">
                        <th colspan="2">
                           USER
                        </th>
                        <th>
                           Email
                        </th>
                        <th>
                           Status
                        </th>
                        <th>
                           Logged at
                        </th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; foreach ($user as $dataUser): ?>
                          <tr>
                            <td>
                              <img src="<?php echo base_url().$dataUser['gambar'] ?>" style="width: 25px;border-radius: 20px;" alt="image">
                            </td>
                            <td>
                               <?php echo $dataUser['nama'] ?>
                            </td>
                            <td>
                              <?php echo $dataUser['email'] ?>
                            </td>
                            <td>
                              <?php if ($dataUser['level'] == 1) {
                                echo '<span class="label label-success">User </span>';
                              }?>
                            </td>
                            <td>
                              <?php echo $dataUser['created_at']; ?>
                              <time class="timeago" datetime="<?php echo $dataUser['created_at']; ?>" title="<?php echo date("j F Y H:i:s", strtotime($dataUser['created_at'])); ?>"></time>
                            </td>
                            </td>                            
                          </tr>
                        <?php endforeach ?>
                      </tbody>                  
                    </table>
                  </div>
                </div>
              </div>
              <!--END TABS-->
            </div>
          </div>
          <!-- END PORTLET-->
        </div>
        <div class="col-md-6 col-sm-6">
          <!-- BEGIN PORTLET-->
          <div class="portlet light">
            <div class="portlet-title tabbable-line">
              <div class="caption caption-md">
                <i class="icon-globe theme-font-color hide"></i>
                <span class="caption-subject theme-font-color bold uppercase">Activities</span>
              </div>              
            </div>
            <div class="portlet-body">
              <!--BEGIN TABS-->
              <div class="tab-content">
                <div class="tab-pane active">
                  <div class="scroller" style="height: 337px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                    <ul class="feeds">
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-bell-o"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 You have 4 pending tasks. <span class="label label-sm label-info">
                                Take action <i class="fa fa-share"></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             Just now
                          </div>
                        </div>
                      </li>
                      <li>
                        <a href="javascript:;">
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-bell-o"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New version v1.4 just lunched!
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             20 mins
                          </div>
                        </div>
                        </a>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-danger">
                                <i class="fa fa-bolt"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Database server #12 overloaded. Please fix the issue.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             24 mins
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New order received and pending for process.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             30 mins
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New payment refund and pending approval.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             40 mins
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-warning">
                                <i class="fa fa-plus"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New member registered. Pending approval.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             1.5 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-bell-o"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Web server hardware needs to be upgraded. <span class="label label-sm label-default ">
                                Overdue </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             2 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-default">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Prod01 database server is overloaded 90%.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             3 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-warning">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New group created. Pending manager review.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             5 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Order payment failed.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             18 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-default">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New application received.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             21 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Dev90 web server restarted. Pending overall system check.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             22 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-default">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New member registered. Pending approval
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             21 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 L45 Network failure. Schedule maintenance.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             22 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-default">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Order canceled with failed payment.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             21 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Web-A2 clound instance created. Schedule full scan.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             22 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-default">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 Member canceled. Schedule account review.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             21 hours
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-bullhorn"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                 New order received. Please take care of it.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                             22 hours
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>                
              </div>
              <!--END TABS-->
            </div>
          </div>
          <!-- END PORTLET-->
        </div>
      </div>
      <!-- END PAGE CONTENT INNER -->
    </div>
  </div>
  <!-- END CONTENT -->
  <script src ="<?php echo base_url() ?>/assets/plugin/jquery.timeago.js"></script>
  <script>
    jQuery(document).ready(function() {
      jQuery("time.timeago").timeago();
    });
  </script>