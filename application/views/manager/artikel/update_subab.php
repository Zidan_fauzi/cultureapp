<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper" id="HalamanManageArtikel">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Update Article <small>Update Article</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Article</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="">Edit Article</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet box grey-cascade">
            <div class="portlet-title">
              <div class="caption">
                <i class="icon-book-open"></i>Managed Article
              </div>
              <a style="display: none;" id="Toast">Show Toast</a>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                <form action="<?php echo base_url('/Manage_Berita/editSubab/').$Subab['subab_id']; ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                  <div class="form-body">
                    <div class="row">
                      <h3 class="col-md-7" style="margin-top: 0px;">Detail Article</h3>
                      <h3 class="col-md-5" style="margin-top: 0px;">Cover Article</h3>
                      <h3 class="form-section"></h3>
                      <div class="col-md-7">
                        <div class="col-md-12">
                          <!--/row-->
                          <div class="row">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Title</b></label>
                                <div class="col-md-9">
                                  <input type="text" class="form-control" value="<?php echo $Subab['subab_title'] ?>" placeholder="Article Subab" name="titleSubab" required>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <!--/span-->
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Category</b></label>
                                <div class="col-md-9">
                                  <select class="select2_category form-control" data-placeholder="Choose a Category" tabindex="1" name="category">
                                    <option value="1" <?php if ($Subab['category_id'] == 1) {echo "selected";}?>>Sosial</option>
                                    <option value="2" <?php if ($Subab['category_id'] == 2) echo "selected";?>>Budaya</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <!--/span-->
                          </div>
                          <!-- /row -->
                          <div class="row">
                            <!--/span-->
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Description</b></label>
                                <div class="col-md-9">
                                  <!-- ckeditor -->
                                  <textarea class=" form-control" name="detailSubab" rows="5" v-model="deskripsi"><?php echo $Subab['subab_description'] ?></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="row">
                          <div class="col-md-9">
                            <div class="form-group">
                              <div class="form-group" id="masukkan">
                                  <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="235">
                                  <span class="help-box">Update your image to make it cover from Article.</span><br>
                                  <input type="button" class="btn red cancel" value="Cancel"/>
                                </div>
                                <div class="form-group" id="tampilan">
                                  <img src='<?php echo base_url($Subab['subab_image']); ?>' style="max-width: 88%;">
                                  <span class="help-box">Update your image to make it cover from Article.</span><br>
                                  <input type="button" class="btn green change" value="Change"/>
                                </div>
                            </div>
                          </div>
                           <!-- /row  -->
                        </div>
                      </div>
                      <!-- <div class="row"></div> -->
                      
                    </div>                        
                    <!--/row -->
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green margin-bottom-10">Update Article</button>                            
                            <a href="<?php echo base_url('Manage_Berita'); ?>" type="submit" class="btn default margin-bottom-10">Back</a>                            
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                      </div>
                    </div>
                  </div>
                </form>
                <div class="col-md-12" style="padding: 0px;">
                  <div class="portlet box green-haze">
                    <div class="portlet-title">
                      <div class="caption">
                        </i>Subab List
                      </div>                              
                    </div>
                    <div class="portlet-body form" style="display: block;">
                      <div class="form-horizontal form-bordered">
                        <div class="form-body">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-12">
                                <a href="<?php echo base_url('Manage_Berita/indexCreate/').$Subab['subab_id'] ?>" style="width: 100%;" class="btn green">
                                    Add New Subab <i class="fa fa-plus"></i>
                                  </a>
                              </div>                  
                            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                              <thead>
                                <tr>
                                  <th class="table-checkbox">
                                    #
                                  </th>
                                  <th>
                                     Subab Title
                                  </th>
                                  <th style="width: 35px;">
                                     Image
                                  </th>
                                  <th>
                                     Total Page
                                  </th>
                                  <th>
                                     Created At
                                  </th>
                                  <th>
                                     Action
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $no = 1; foreach ($artikel as $dataArtikel): ?>
                                  <tr class="odd gradeX">
                                    <td>
                                      #<?php echo $no++; ?>
                                    </td>
                                    <td>
                                       <?php echo $dataArtikel['article_title'] ?>
                                    </td>
                                    <td style="width: 35px;">
                                      <img src="<?php echo base_url().$dataArtikel['article_image'] ?>" alt="image" style="width: 100%;">
                                    </td>
                                    </td>
                                    <td>
                                       <?php echo $dataArtikel['page_number'] ?>
                                    </td>
                                    </td>
                                    <td>
                                       <?php echo $dataArtikel['created_at'] ?>
                                    </td>
                                    </td>
                                    <td>
                                      <a class="btn green" href="<?php echo base_url('Manage_Berita/indexEdit/').$dataArtikel['article_id'] ?>">Edit</a>
                                      <a class="btn red delete-button" data-toggle="modal" href="#draggable" data-id = '<?php echo $dataArtikel['article_id']; ?>' data-url = "<?php echo base_url('Manage_Berita'); ?>" data-judul='<?php echo $dataArtikel['article_title']; ?>'>Delete</a>
                                    </td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- /.modal -->
    <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Start Dragging Here</h4>
          </div>
          <div class="modal-body" id="modalDeleteJudul"></div>
          <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
            <a type="button" class="btn red" id="modalDelete">Delete</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <script type="text/javascript">
    var status = '<?php echo $this->session->flashdata('status'); ?>';
    var method = '<?php echo $this->session->flashdata('method'); ?>';
    $('#Toast').on('click', function () {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      toastr.success("Success "+method+" Subab <?php echo $this->session->flashdata('ArticleName'); ?>", "Success "+method+" Subab !");
    });
    if (status  == 'success') {
      $('#Toast').click();
    }else {
      console.log('gagal');
    }
  </script>
  <script type="text/javascript">
    $('.delete-button').click(function(){
        var id=$(this).attr('data-id');          
        var base_url = $(this).attr('data-url');
        var judul = $(this).attr('data-judul');
        $('#modalDelete').attr('href',base_url+'/delete/'+id);
        document.getElementById("modalDeleteJudul").innerHTML = "Apakah Kamu yakin ingin menghapus Subab <a href="+base_url+"/indexEdit/"+id+"><strong>"+judul+"</strong></a> ?";
    })
  </script>
  <script type="text/javascript">
    $("#masukkan").hide();             
    $(".cancel").hide();   
              
    $(".cancel").click(function(){
        $("#masukkan").hide();
        $("#tampilan").show();
        $(".change").show();
        $(".cancel").hide(); 
    });

    $(".change").click(function(){
        $("#masukkan").show();
        $("#tampilan").hide();     
        $(".cancel").show(); 
        $(".change").hide();  
    });
  </script>