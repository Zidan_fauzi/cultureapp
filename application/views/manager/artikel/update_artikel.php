<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Update Subab <small>form Update new Subab</small></h1>
        </div>
        <!-- END PAGE TITLE -->       
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Article</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita/indexEditSubab/').$article['subab_id']; ?>">Edit Article</a>
                <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="">Update Subab</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row" id="HalamanArtikel">
        <div class="col-md-12">
          <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">            
            <div class="tab-pane active" id="tab_2">
              <div class="portlet box green">
                <div class="portlet-title">
                  <div class="caption">
                    Update Subab
                  </div>
                </div>
                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form action="<?php echo base_url('/Manage_Berita/Edit/').$article['article_id']; ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                      <div class="row">
                        <h3 class="col-md-7" style="margin-top: 0px;">Detail Subab</h3>
                        <h3 class="col-md-5" style="margin-top: 0px;">Cover Subab</h3>
                        <h3 class="form-section"></h3>
                        <div class="col-md-7">
                          <div class="col-md-12">
                            <!--/row-->
                            <div class="row">
                              <div class="col-md-10">
                                <div class="form-group">
                                  <label class="control-label col-md-3"><b>Title</b></label>
                                  <div class="col-md-9">
                                    <input type="text" class="form-control" value="<?php echo $article['article_title'] ?>" placeholder="Article Title" name="title" required>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /row -->
                            <div class="row">
                              <!--/span-->
                              <div class="col-md-10">
                                <div class="form-group">
                                  <label class="control-label col-md-3"><b>Total Page</b></label>
                                  <div class="col-md-9">
                                    <select class="form-control" v-model="banyakHalaman" v-on:change="addPageData($event)" name="TotalPage">
                                      <option :value="1" >1</option>
                                      <option :value="2" >2</option>
                                      <option :value="3" >3</option>
                                    </select>                               
                                  </div>
                                </div>
                              </div>
                            </div> 
                            <!-- /row -->
                            <div class="row">
                              <!--/span-->
                              <div class="col-md-10">
                                <div class="form-group">
                                  <label class="control-label col-md-3"><b>Description</b></label>
                                  <div class="col-md-9">
                                    <!-- ckeditor -->
                                    <textarea class=" form-control" name="descArtikel" rows="10"><?php echo $article['article_description'] ?></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>                             
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="row">
                            <div class="col-md-9">
                              
                              <div class="form-group" id="masukkan">
                                <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="235">
                                <span class="help-box">Update your image to make it cover from subab.</span>
                                <input type="button" class="btn red cancel" value="Cancel"/>
                              </div>
                              <div class="form-group" id="tampilan">
                                <img src='<?php echo base_url($article['article_image']); ?>' style="max-width: 88%;">
                                <span class="help-box">Update your image to make it cover from subab.</span>
                                <input type="button" class="btn green change" value="Change"/>
                              </div>
                            </div>
                             <!-- /row  -->
                          </div>
                        </div>
                        <!-- <div class="row"></div> -->
                        
                      </div>
                      <h3 class="form-section">Subab Page</h3>
                      <input type="hidden" name="TotalPage" :value="banyakHalaman">
                      <!-- /row -->
                      <div class="row">
                        <div class="col-md-12">
                          <div class="portlet box green-haze" v-for="index in Desc">
                            <div class="portlet-title">
                              <div class="caption">
                               Page {{index.number + 1}}
                              </div>
                              <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                              </div>                                  
                            </div>
                            <div class="portlet-body form" style="display: block;">
                              <div class="form-horizontal form-bordered"> 
                                <div class="form-body">
                                  <div class="form-group">
                                    <input type="hidden" :name="'detail_type'+index" value="description">
                                    <div class="col-lg-9 col-md-10">
                                      
                                      <textarea class="ckeditor form-control" :id="'editor'+index.number" :name="'page_content'+index.number" rows="6">{{index.content}}</textarea>
                                      
                                      <input type="hidden" name="detail_type" value="description">

                                    </div>
                                    <div class=" col-lg-3 col-md-12">
      																<div class="portlet box green-haze">
      																	<div class="portlet-title">
      																		<div class="caption">
      																			Preview
      																		</div>
      																	</div>
      																	<div class="portlet-body form" style="display: block;">
      																		<div class="form-horizontal form-bordered">
      																			<div class="form-body">
      																				<div class="form-group">
      																					<div :id="'resulteditor'+index.number" style="padding: 0;min-height: 240px; max-height: 440px; overflow-y: auto">
      																						<!-- hasil deskripsi -->
      																					</div>
      																				</div>
      																			</div>
      																		</div>
      																	</div>
      																</div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/row -->
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">Submit</button>
                              <a type="button" class="btn default" href="<?php echo base_url('Manage_Berita/indexEditSubab/').$article['subab_id']; ?>">Cancel</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                      </div>
                    </div>
                  </form>
                  <!-- END FORM-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- <?php // echo $article_page_detail['{{index}}']['page_detail_content'];?> -->
  <script type="text/javascript">
    window.vo = new Vue({
        el: '#HalamanArtikel',
        name: 'HalamanArtikel',
        data() {
            return {
              title: '',
              banyakHalaman: <?php echo $article['page_number'] ?>,
              halaman: 1,
              typedata: '',
              Desc: []            
            }
        },
        beforeMount(){
          $.ajax({
            type: "POST",
            url: "<?php echo base_url('Manage_Berita/indexPageDetail'); ?>",
            dataType: "json",
            data: {
                id:  <?php echo $article['page_id'] ?>
            },
            success: function (data) {
              vo.Desc = data;
            }
          });
        },
        updated(){
          this.render()
        },
        methods: {
          setTypeData: function(e) {
            console.log(e.target.value)
            this.typedata = e.target.value            
          },
          addPageData: function(e) {
            if (e.target.value == 1) {
              this.satuHalman()
            }else if (e.target.value == 2) {
              this.duaHalman()
            }else if (e.target.value == 3) {
              this.tigaHalman()
            }
          },
          satuHalman() {
            this.banyakHalaman = 1
            this.banyakHalaman += 0
            this.deleteRow(1)
            this.deleteEditor(0)
          },
          duaHalman() {
            console.log('masuk')
            this.banyakHalaman = 1
            this.banyakHalaman += 1
            this.deleteRow(2) 
            this.addRow(1)
            if (this.Desc.length > 2) {
              this.deleteEditor(1)
              console.log('masuk')
              this.deleteRow(2)
            }
            this.deleteEditor(0)
          },
          tigaHalman() {
            this.banyakHalaman = 1
            this.banyakHalaman += 2
              console.log(this.Desc.length)
            if (this.Desc.length < 2) {
              console.log('masuk')
              this.Desc.push({number: 1, content: ''})
              this.deleteEditor(0)
            }else if (this.Desc.length == 2) {
              this.deleteEditor(0)
              this.deleteEditor(1)
            }
            this.addRow(2)
          },
          addRow(Page) {
            this.Desc.push({number: Page, content: ''})
          },
          deleteRow(index){
            if (this.Desc.length > 2) {
              if (index == 1) {
                this.Desc.splice(2, 1)  
              }
            }
            this.Desc.splice(index, 1)
          },
          deleteEditor(i){
            CKEDITOR.instances["editor"+i].destroy()
          },
          render() {  
            var page = this.Desc.length;
              for(var i = 0; i < page; i++) {
                var name = "editor" + i

                CKEDITOR.replace("editor"+i)
                var value = CKEDITOR.instances[name].getData()
                var htmltags = htmltags = `
                    <html>
                      <head><meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
                      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,700&display=swap" rel="stylesheet">
                      <style>
                      #mobile-display  {
                          padding: 40px 32px 12px 32px;
                      }
                      #mobile-display * {
                        font-family: 'Poppins', sans-serif;
                      }

                      #mobile-display h1, #mobile-display  h2, #mobile-display  h3 {
                        font-size: 36px;
                        font-weight: 800;
                        line-height: 40px;
                        margin: 0;
                      }

                      #mobile-display > *:first-child img {
                          border-radius: 0;
                          margin: -40px -32px 0px -32px;
                          width: calc(100% + 64px) !important;
                      }

                      #mobile-display h4, #mobile-display h5, #mobile-display h6 {
                        font-size: 16px;
                        font-weight: 400;
                        line-height: 24px;
                        margin: 0;
                      }

                      #mobile-display img {
                        width: 100%;
                        border-radius: 20px;
                      }

                      #mobile-display blockquote {
                          background-color: #5F6CAF;
                          color: #FFFFFF;
                          font-size: 14px;
                          font-weight: 400;
                          line-height: 20px;
                          border: none;
                          padding: 16px 20px 16px 44px;
                          position: relative;
                          margin: 4px -32px 12px -32px;

                      }

                      #mobile-display blockquote:after {
                          content: "";
                          position: absolute;
                          top: 12px;
                          left: 32px;
                          bottom: 12px;
                          width: 2px;
                          background-color: #FFFFFF;
                      }

                      #mobile-display address {
                          font-size: 12px;
                          line-height: 20px;
                          font-weight: 300;
                          font-style: italic;
                      }

                      #mobile-display p { 
                        font-size: 14px;
                        line-height: 20px;
                      }
                      </style></head>
                      <body><div id="mobile-display">` + value + `</div></body>
                    </html>
                  `;
                
                document.getElementById("result"+name).innerHTML = htmltags;

                CKEDITOR.instances[name].on('change', function() {
                  var value = this.getData();
                  var name = this.name;
                  $.get("<?php echo base_url('assets/customMobileStyle.html') ?>", function(html_string)
                  {
                    var htmltags = `
                    <html>
                      <head>` + html_string + `</head>
                      <body><div id="mobile-display">` + value + `</div></body>
                    </html>
                    `;
                    document.getElementById("result"+ name).innerHTML = htmltags;
                  },'html'); 
                });
              }
            let EditorScript = document.createElement('script') 
            EditorScript.setAttribute('src', '../../assets/global/plugins/ckeditor/ckeditor.js') 
            document.head.appendChild(EditorScript)
          }
        }
    });
  </script>
  <script type="text/javascript">
    $("#masukkan").hide();             
    $(".cancel").hide();   
              
    $(".cancel").click(function(){
        $("#masukkan").hide();
        $("#tampilan").show();
        $(".change").show();
        $(".cancel").hide(); 
    });

    $(".change").click(function(){
        $("#masukkan").show();
        $("#tampilan").hide();     
        $(".cancel").show(); 
        $(".change").hide();  
    });
  </script>