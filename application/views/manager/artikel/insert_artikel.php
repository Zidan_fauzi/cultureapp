<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Insert Subab <small>form insert new Subab</small></h1>
				</div>
				<!-- END PAGE TITLE -->				
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
		        <li>
		          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
		          <i class="fa fa-circle"></i>
		        </li>
		        <li>
		          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Article</a>
		          <i class="fa fa-circle"></i>
		        </li>
				<li>
					<a href="<?php echo base_url('Manage_Berita/indexEditSubab/').$Subab['subab_id']; ?>">Edit Article</a>
		          	<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="">Insert New Subab</a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row" id="HalamanArtikel">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">						
						<div class="tab-pane active" id="tab_2">
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										Insert Subab
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form action="<?php echo base_url('/Manage_Berita/Create/').$Subab['subab_id']; ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
										<div class="form-body">
											<div class="row">
												<h3 class="col-md-7" style="margin-top: 0px;">Detail Subab</h3>
												<h3 class="col-md-5" style="margin-top: 0px;">Cover Subab</h3>
												<h3 class="form-section"></h3>
												<div class="col-md-7">
													<div class="col-md-12">
														<!--/row-->
														<div class="row">
															<div class="col-md-10">
																<div class="form-group">
																	<label class="control-label col-md-3"><b>Title</b></label>
																	<div class="col-md-9">
																		<input type="text" class="form-control" v-model="title" placeholder="Subab Title" name="title" required>
																	</div>
																</div>
															</div>
														</div>
														<!-- /row -->
														<div class="row">
															<!--/span-->
															<div class="col-md-10">
																<div class="form-group">
																	<label class="control-label col-md-3"><b>Total Page</b></label>
																	<div class="col-md-9">
																		<select class="form-control" v-model="banyakHalaman" name="TotalPage">
																			<option :value="1" @click="satuHalman()">1</option>
																			<option :value="2" @click="duaHalman()">2</option>
																			<option :value="3" @click="tigaHalman()">3</option>
																		</select>																
																	</div>
																</div>
															</div>
														</div>
														<!-- /row -->
														<div class="row">
								                            <!--/span-->
								                            <div class="col-md-10">
								                              <div class="form-group">
								                                <label class="control-label col-md-3"><b>Description</b></label>
								                                <div class="col-md-9">
								                                  <!-- ckeditor -->
								                                  <textarea class=" form-control" name="descArtikel" rows="10"></textarea>
								                                </div>
								                              </div>
								                            </div>
								                          </div>
													</div>
												</div>
												<div class="col-md-5">
													<div class="row">
														<div class="col-md-9">
															<div class="form-group">
																<input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="250" required>
																<span class="help-box">Insert your image to make it cover from Subab.</span>
															</div>
														</div>
														 <!-- /row  -->
													</div>
												</div>
												<!-- <div class="row"></div> -->
												
											</div>
											<h3 class="form-section">Subab Page</h3>
											<input type="hidden" name="TotalPage" :value="banyakHalaman">
											<!-- /row -->
											<div class="row">
												<div class="col-md-12">
													<div class="portlet box green-haze" v-for="index in banyakHalaman">
														<div class="portlet-title">
															<div class="caption">
																Page {{index}}
															</div>
															<div class="tools">
																<a href="javascript:;" class="collapse">
																</a>
															</div>																	
														</div>
														<div class="portlet-body form" style="display: block;">
															<div class="form-horizontal form-bordered">	
																<div class="form-body">
																	<div class="form-group">
																		<input type="hidden" :name="'detail_type'+index" value="description">

																		<div class="col-lg-9 col-md-10">
																			<textarea class="ckeditor form-control" :id="'editor'+index" :name="'page_content'+index" rows="10" v-model="deskripsi"></textarea>
																		</div>

																		<div class=" col-lg-3 col-md-12">
																			<div class="portlet box green-haze">
																				<div class="portlet-title">
																					<div class="caption">
																						</i>Preview
																					</div>															
																				</div>
																				<div class="portlet-body form" style="display: block;">
																					<div class="form-horizontal form-bordered">
																						<div class="form-body">
																							<div class="form-group">
																								<div :id="'resulteditor'+index" style="padding: 0;min-height: 240px; max-height: 440px; overflow-y: auto">
																									<!-- hasil deskripsi -->
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--/row -->
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn green">Submit</button>
															<a type="button" class="btn default" href="<?php echo base_url('Manage_Berita/indexEditSubab/').$Subab['subab_id']; ?>">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-6">
												</div>
											</div>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
    
	<script type="text/javascript">
		var vo = new Vue({
		    el: '#HalamanArtikel',
		    name: 'HalamanArtikel',
		    data() {
		        return {
		        	title: '',
		            banyakHalaman: 1,
		            deskripsi: '',
		            typedata: '',
		            halaman: 1            
		        }
		    },
		    beforeMount(){
		    	this.render()
		    },
		    updated(){
		    	this.render()
		    },
		    methods: {
		    	setTypeData: function(e) {
		    		console.log(e.target.value)
		    		this.typedata = e.target.value		    		
		    	},
		        satuHalman() {
		        	this.banyakHalaman = 1
		            this.banyakHalaman += 0
		        },
		        duaHalman() {
		        	this.banyakHalaman = 1
		            this.banyakHalaman += 1
		        },
		        tigaHalman() {
		        	this.banyakHalaman = 1
		            this.banyakHalaman += 2
		        },
		        render() {
		        	if (this.banyakHalaman != 1) {
		        		for (var i = 2; i <= this.banyakHalaman; i++) {
			              CKEDITOR.replace("editor"+i)
			            }
		        	}
		        	let EditorScript = document.createElement('script') 
			    	EditorScript.setAttribute('src', '../assets/global/plugins/ckeditor/ckeditor.js') 
			    	document.head.appendChild(EditorScript)

					setTimeout(function () {
						var page = this.banyakHalaman;
						for(var i = 1; i <= page; i++) {
							var name = "editor" + i;
							console.log(name);
							CKEDITOR.instances[name].on('change', function() {
								var value = this.getData();
								var name = this.name;
								$.get("<?php echo base_url('assets/customMobileStyle.html') ?>", function(html_string)
								{
									var htmltags = `
									<html>
										<head>` + html_string + `</head>
										<body><div id="mobile-display">` + value + `</div></body>
									</html>
									`;
									document.getElementById("result"+ name).innerHTML = htmltags;
								},'html'); 
							});
						}
					}.bind(this), 1000)
			    }
		    }
		});
	</script>