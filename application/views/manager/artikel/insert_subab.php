<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper" id="HalamanManageArtikel">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Insert Article <small>managed Article</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Article</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="">Insert Article</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet box green">
            <div class="portlet-title">
              <div class="caption">
                <i class="icon-book-open"></i>Insert Article
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                <form action="<?php echo base_url(); ?>/Manage_Berita/CreateSubab" class="form-horizontal" method="post" enctype="multipart/form-data">
                  <div class="form-body">
                    <div class="row">
                      <h3 class="col-md-7" style="margin-top: 0px;">Detail Article</h3>
                      <h3 class="col-md-5" style="margin-top: 0px;">Cover Article</h3>
                      <h3 class="form-section"></h3>
                      <div class="col-md-7">
                        <div class="col-md-12">
                          <!--/row-->
                          <div class="row">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Title</b></label>
                                <div class="col-md-9">
                                  <input type="text" class="form-control" v-model="title" placeholder="Article Title" name="titleSubab" required>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <!--/span-->
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Category</b></label>
                                <div class="col-md-9">
                                  <select class="select2_category form-control" data-placeholder="Choose a Category" tabindex="1" name="category">
                                    <option value="1">Sosial</option>
                                    <option value="2">Budaya</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <!--/span-->
                          </div>
                           <!-- /row -->
                          <div class="row">
                            <!--/span-->
                            <div class="col-md-10">
                              <div class="form-group">
                                <label class="control-label col-md-3"><b>Description</b></label>
                                <div class="col-md-9">
                                  <!-- ckeditor -->
                                  <textarea class=" form-control" name="detailSubab" rows="10" v-model="deskripsi"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="row">
                          <div class="col-md-9">
                            <div class="form-group">
                              <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="250" required>
                              <span class="help-box">Insert your image to make it cover from article.</span>
                            </div>
                          </div>
                           <!-- /row  -->
                        </div>
                      </div>
                      <!-- <div class="row"></div> -->
                      
                    </div>                        
                    <!--/row -->
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Save Article</button>                            
                            <a type="submit" class="btn default" href="<?php echo base_url('Manage_Berita') ?>">Cancel</a>                
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- /.modal -->
    <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Start Dragging Here</h4>
          </div>
          <div class="modal-body" id="modalDeleteJudul"></div>
          <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
            <a type="button" class="btn red" id="modalDelete">Delete</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <script type="text/javascript">
    var status = '<?php echo $this->session->flashdata('status'); ?>';
    var method = '<?php echo $this->session->flashdata('method'); ?>';
    $('#Toast').on('click', function () {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      toastr.success("Success "+method+" Article <?php echo $this->session->flashdata('ArticleName'); ?>", "Success "+method+" Article !");
    });
    if (status  == 'success') {
      $('#Toast').click();
    }else {
      console.log('gagal');
    }
  </script>
  <script type="text/javascript">
    $('.delete-button').click(function(){
        var id=$(this).attr('data-id');          
        var base_url = $(this).attr('data-url');
        var judul = $(this).attr('data-judul');
        $('#modalDelete').attr('href',base_url+'/delete/'+id);
        document.getElementById("modalDeleteJudul").innerHTML = "Apakah Kamu yakin ingin menghapus Artikel <a href="+base_url+"/indexEdit/"+id+"><strong>"+judul+"</strong></a> ?";
    })
  </script>