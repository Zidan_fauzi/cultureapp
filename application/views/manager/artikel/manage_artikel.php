<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper" id="HalamanManageArtikel">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Managed Artikel <small>managed Artikel</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Artikel</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet box grey-cascade">
            <div class="portlet-title">
              <div class="caption">
                <i class="icon-book-open"></i>Managed Artikel
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                <div class="row">
                  <div class="col-md-6">
                    <div class="btn-group">
                      <a href="<?php echo base_url() ?>Manage_Berita/indexSubab" id="sample_editable_1_new" class="btn green">
                        Add New <i class="fa fa-plus"></i>
                      </a>
                      <a style="display: none;" id="Toast">Show Toast</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;">
                          Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                  <tr>
                    <th class="table-checkbox">
                      #
                    </th>
                    <th>
                       Artikel Title
                    </th>
                    <th>
                       Category
                    </th>
                    <th style="width: 35px;">
                       Artikel Image
                    </th>
                    <th>
                       Created At
                    </th>
                    <th>
                       Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($Subab as $dataSubab): ?>
                    <tr>
                      <td>
                        #<?php echo $no++; ?>
                      </td>
                      <td>
                         <?php echo $dataSubab['subab_title'] ?>
                      </td>
                      <td>
                        <?php echo $dataSubab['category_name'] ?>
                      </td>
                      </td>
                      <td style="width: 35px;">
                         <img src="<?php echo base_url().$dataSubab['subab_image'] ?>" alt="image" style="width: 100%;">
                      </td>
                      </td>
                      <td>
                         <?php echo $dataSubab['created_at'] ?>
                      </td>
                      </td>
                      <td>
                        <a class="btn green" href="<?php echo base_url('Manage_Berita/indexEditSubab/').$dataSubab['subab_id'] ?>">Edit</a>
                        <a class="btn red delete-button" data-toggle="modal" href="#draggable" data-id = '<?php echo $dataSubab['subab_id']; ?>' data-url = "<?php echo base_url('Manage_Berita'); ?>" data-judul='<?php echo $dataSubab['subab_title']; ?>'>Delete</a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- /.modal -->
    <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Start Dragging Here</h4>
          </div>
          <div class="modal-body" id="modalDeleteJudul"></div>
          <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
            <a type="button" class="btn red" id="modalDelete">Delete</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <script type="text/javascript">
    var status = '<?php echo $this->session->flashdata('status'); ?>';
    var method = '<?php echo $this->session->flashdata('method'); ?>';
    $('#Toast').on('click', function () {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      toastr.success("Success "+method+" Artikel <?php echo $this->session->flashdata('ArticleName'); ?>", "Success "+method+" Artikel !");
    });
    if (status  == 'success') {
      $('#Toast').click();
    }else {
      console.log('gagal');
    }
  </script>
  <script type="text/javascript">
    $('.delete-button').click(function(){
        var id=$(this).attr('data-id');          
        var base_url = $(this).attr('data-url');
        var judul = $(this).attr('data-judul');
        $('#modalDelete').attr('href',base_url+'/deleteSubab/'+id);
        document.getElementById("modalDeleteJudul").innerHTML = "Apakah Kamu yakin ingin menghapus Artikel <a href="+base_url+"/indexEditSubab/"+id+"><strong>"+judul+"</strong></a> ?";
    })
  </script>