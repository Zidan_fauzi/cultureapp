<!-- BEGIN CONTENT -->
  <div class="page-content-wrapper" id="HalamanManageArtikel">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Managed Article <small>managed article</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="<?php echo base_url('Manage_Berita'); ?>">Managed Article</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet box grey-cascade">
            <div class="portlet-title">
              <div class="caption">
                <i class="icon-book-open"></i>Managed Article
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                <div class="row">
                  <div class="col-md-6">
                    <div class="btn-group">
                      <a href="<?php echo base_url() ?>Manage_User/indexCreate" id="sample_editable_1_new" class="btn green">
                        Add New <i class="fa fa-plus"></i>
                      </a>
                      <a style="display: none;" id="Toast">Show Toast</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;">
                          Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                  <tr>
                    <th>
                      #
                    </th>
                    <th>
                       User
                    </th>
                    <th>
                       Email
                    </th>
                    <th>
                       Status
                    </th>
                    <th>
                       Created At
                    </th>
                    <th>
                       Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($user as $dataUser): ?>
                    <tr class="odd gradeX">
                      <td>
                        #<?php echo $no++; ?>
                      </td>
                      <td style="display: block;">
                        <img src="<?php echo base_url().$dataUser['gambar'] ?>" style="width: 25px;border-radius: 20px;" alt="image">
                        &nbsp&nbsp<?php echo $dataUser['nama'] ?>
                      </td>
                      <td>
                        <?php echo $dataUser['email'] ?>
                      </td>
                      <td>
                        <?php if ($dataUser['level'] == 1) {
                          echo '<span class="label label-success">User </span>';
                        }?>
                      </td>
                      <td>
                         <?php echo $dataUser['created_at'] ?>
                      </td>
                      </td>
                      <td>
                        <a class="btn red delete-button" data-toggle="modal" href="#draggable" data-id = '<?php echo $dataUser['id_admin']; ?>' data-url = "<?php echo base_url('Manage_User'); ?>" data-judul='<?php echo $dataUser['nama']; ?>'>Delete</a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- /.modal -->
    <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Start Dragging Here</h4>
          </div>
          <div class="modal-body" id="modalDeleteJudul"></div>
          <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
            <a type="button" class="btn red" id="modalDelete">Delete</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <script type="text/javascript">
    var status = '<?php echo $this->session->flashdata('status'); ?>';
    var method = '<?php echo $this->session->flashdata('method'); ?>';
    $('#Toast').on('click', function () {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      toastr.success("Success "+method+" User <?php echo $this->session->flashdata('email'); ?>", "Success "+method+" User !");
    });
    if (status  == 'success') {
      $('#Toast').click();
    }else {
      console.log('gagal');
    }
  </script>
  <script type="text/javascript">
    $('.delete-button').click(function(){
        var id=$(this).attr('data-id');          
        var base_url = $(this).attr('data-url');
        var judul = $(this).attr('data-judul');
        $('#modalDelete').attr('href',base_url+'/delete/'+id);
        document.getElementById("modalDeleteJudul").innerHTML = "Are you Sure to Delete User <strong>"+judul+"</strong> ?";
    })
  </script>