<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Insert User <small>form insert new User</small></h1>
				</div>
				<!-- END PAGE TITLE -->				
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
		        <li>
		          <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
		          <i class="fa fa-circle"></i>
		        </li>
		        <li>
		          <a href="<?php echo base_url('Manage_User'); ?>">Managed User</a>
		          <i class="fa fa-circle"></i>
		        </li>
				<li>
					<a href="#">Insert User</a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">						
						<div class="tab-content">
							<div class="tab-pane active" id="tab_2">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											Insert User
											<a style="display: none;" id="Toast">Show Toast</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="<?php echo base_url(); ?>/Manage_User/Create" class="form-horizontal" method="post" enctype="multipart/form-data">
											<div class="form-body">
												<div class="row">
													<h3 class="col-md-7" style="margin-top: 0px;">Biodata</h3>
													<h3 class="col-md-5" style="margin-top: 0px;">Foto Profiler</h3>
													<h3 class="form-section"></h3>
													<div class="col-md-7">
														<div class="col-md-12">
															<!--/row-->
															<div class="row">
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="control-label col-md-3"><b>Username</b></label>
																		<div class="col-md-9">
																			<input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $this->session->flashdata('nama'); ?>" required>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /row -->
															<!--/row-->
															<div class="row">
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="control-label col-md-3"><b>Email</b></label>
																		<div class="col-md-9">
																			<input type="text" value="<?php echo $this->session->flashdata('email'); ?>" class="form-control" placeholder="email" name="email" required>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /row -->
															<!--/row-->
															<div class="row">
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="control-label col-md-3"><b>Password</b></label>
																		<div class="col-md-9">
																			<input type="text" class="form-control" placeholder="password" name="password" value="<?php echo $this->session->flashdata('password'); ?>" required>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /row -->
														</div>
													</div>
													<div class="col-md-5">
														<div class="row">
															<div class="col-md-9">
																<div class="form-group">
																	<input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="250">
																	<span class="help-box">Insert your image to make it Profile Photo from User.</span>
																</div>
															</div>
															 <!-- /row  -->
														</div>
													</div>
													<!-- <div class="row"></div> -->
													
												</div>												
												<!--/row -->
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn green">Submit</button>
																<a type="button" class="btn default" href="<?php echo base_url('Manage_Berita') ?>">Cancel</a>
															</div>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<script type="text/javascript">
	    var status = '<?php echo $this->session->flashdata('status'); ?>';
	    var method = '<?php echo $this->session->flashdata('method'); ?>';
	    $('#Toast').on('click', function () {
	      toastr.options = {
	        "closeButton": true,
	        "debug": false,
	        "positionClass": "toast-bottom-right",
	        "onclick": null,
	        "showDuration": "1500",
	        "hideDuration": "1500",
	        "timeOut": "5000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut"
	      }
	      toastr.error("This email has already taken ", "failed "+method+" User !");
	    });
	    if (status  == 'failed') {
	      $('#Toast').click();
	    }else {
	      console.log('gagal');
	    }
  	</script>