<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
             Widget settings form goes here
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue">Save changes</button>
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Insert Quiz <small>form insert new quiz</small></h1>
      </div>
      <!-- END PAGE TITLE -->       
    </div>
    <!-- END PAGE HEAD -->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
          <li>
            <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <a href="<?php echo base_url('Manage_Quiz'); ?>">Managed Quiz</a>
            <i class="fa fa-circle"></i>
          </li>
      <li>
        <a href="#">Insert Quiz</a>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row" id="HalamanArtikel">
      <div class="col-md-12">
        <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">            
          <div class="tab-content">
            <div class="tab-pane active" id="tab_2">
              <div class="portlet box green">
                <div class="portlet-title">
                  <div class="caption">
                    Insert Quiz
                  </div>
                </div>
                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form action="<?php echo base_url('/Manage_Quiz/Edit/'.$quiz['article_id']); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                      <div class="row">
                        <h3 class="col-md-10" style="margin-top: 0px;">Detail Quiz</h3>
                        <h3 class="form-section"></h3>
                        <div class="col-md-10">
                          <div class="col-md-12">
                            <!-- /row -->
                            <div class="row">
                              <!--/span-->
                              <div class="col-md-10">
                                <div class="form-group">
                                  <label class="control-label col-md-3"><b>Article</b></label>
                                  <div class="col-md-9">
                                    <select class="select2_category form-control" name="articleId">
                                      <?php foreach ($artikel as $DataArtikel): ?>
                                        <option value="<?php echo $DataArtikel['article_id'] ?>" name="articleId"
                                          <?php if ($quiz['article_id'] == $DataArtikel['article_id']){ echo "selected"; } ?>
                                        >
                                          <?php echo $DataArtikel['article_title'] ?>
                                        </option>
                                      <?php endforeach ?>
                                    </select>                       
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3"><b>Total Answer</b></label>
                                  <div class="col-md-9">
                                    <div id="spinner4">
                                      <div class="input-group" style="width:150px;">
                                        <div class="spinner-buttons input-group-btn">
                                          <a @click="kurangiJawaban" type="button" class="btn spinner-down red">
                                          <i class="fa fa-minus"></i>
                                          </a>
                                        </div>
                                        <input type="text" v-model="banyakJawaban" class="spinner-input form-control" readonly>
                                        <div class="spinner-buttons input-group-btn">
                                          <a @click="tambahJawaban" type="button" class="btn spinner-up blue">
                                          <i class="fa fa-plus"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>      
                                  </div>
                                </div>
                              </div>
                              <!--/span-->
                            </div>
                          </div>
                        </div>
                        <!-- <div class="row"></div> -->
                        
                      </div>
                      <h3 class="form-section">Quiz Question</h3>
                      <input type="hidden" name="banyakSoal" :value="banyakSoal" required>
                      <input type="hidden" name="banyakJawaban" :value="banyakJawaban" required>
                      <!-- /row -->
                      <div class="row">
                        <div class="col-md-12">
                          <!-- BEGIN EXTRAS PORTLET-->
                          <div class="portlet box green-haze" v-for="(index) in Quiz">
                            <div class="portlet-title">
                              <div class="caption">
                                Question {{index.number + 1}}
                              </div>
                              <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                  <a v-if="banyakSoal2 == index.number && index.number != 0" @click="kurangiSoal(index.number)" class="remove"></a>
                              </div>
                            </div>
                            <div class="portlet-body form" style="display: block;">
                              <div class="form-horizontal form-bordered">
                                <div class="form-body">
                                  <div class="form-group">
                                    <label class="control-label col-md-3"><b>Question</b></label>
                                    <div class="col-md-9">
                                      <textarea :name="'quiz_question'+index.number" class="form-control">{{index.quiz_content}}</textarea>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-body">
                                  <div class="form-group">
                                    <label class="control-label col-md-3"><b>Answer</b></label>
                                    <div class="col-md-9">
                                      <!-- state="true" -->
                                      <div v-for="(index2) in index.quizAnswer">
                                        <div class="margin-bottom-10" style="display: block ruby;">
                                          <input type="radio" :name="'AnswerKey'+index.number" :value="index2.number" v-if="index.quiz_answer_key == index2.quiz_answer_key" style="width: 40px;" checked></input>
                                          <input type="radio" style="width: 40px;" :name="'AnswerKey'+index.number" :value="index2.quiz_answer_key" v-else ></input>

                                          <label v-if="index2.quiz_answer_key == 1" for="option1">A.</label>
                                          <label v-if="index2.quiz_answer_key == 2" for="option1">B.</label>
                                          <label v-if="index2.quiz_answer_key == 3" for="option1">C.</label>
                                          <label v-if="index2.quiz_answer_key == 4" for="option1">D.</label>

                                          <input type="text" class="form-control" :name="'answerQuestion'+index2.quiz_answer_key" :value="index2.quiz_answer_content" style="width: 75%; height: 30px;" v-if="index2.quiz_answer_content != ''" required>
                                          <input type="text" class="form-control" :name="'answerQuestion'+index2.quiz_answer_key" style="width: 75%; height: 30px;" v-else required>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a class="btn green col-md-12" @click="tambahSoal">Tambah Soal</a>
                        </div>
                      </div>
                      
                      <!--/row -->
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">Submit</button>
                              <a class="btn default" href="<?php echo base_url('Manage_Quiz') ?>">Cancel</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                      </div>
                    </div>
                  </form>
                  <!-- END FORM-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END PAGE CONTENT-->
  </div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
  var vo = new Vue({
      el: '#HalamanArtikel',
      name: 'HalamanArtikel',
      data() {
          return {
            title: '',
            banyakJawaban: <?php echo $quiz['quiz_total_answer'] ?>,
            halaman: 1,
            Quiz: [],
            banyakSoal: <?php echo $quiz['article_id'] ?>,
            banyakSoal2: 0
          }
      },
      beforeMount(){
        this.render(true)
        jQuery(document).ready(function() {  
          Metronic.init();
        });
        $.ajax({
          type: "POST",
          url: "<?php echo base_url('Manage_Quiz/IndexQuiz'); ?>",
          dataType: "json",
          data: {
              id: <?php echo $quiz['article_id'] ?>
          },
          success: function (data) {
            console.log(data)
            vo.Quiz = data
          }
        });
      },
      updated(){
        this.banyakSoal = this.Quiz.length
        this.render()
      },
      methods: {
          tambahSoal() {
            this.Quiz.push({
              number: this.banyakSoal, 
              quiz_content: '', 
              quiz_answer_key:'0', 
              quizAnswer: 
              [{ 
                  quiz_answer_content: '', 
                  quiz_answer_key:'1'   
                },
                { 
                  quiz_answer_content: '', 
                  quiz_answer_key:'2'   
                },
                { 
                  quiz_answer_content: '', 
                  quiz_answer_key:'3'   
                },
                { 
                  quiz_answer_content: '', 
                  quiz_answer_key:'4'   
                }]
            })
            this.Quiz.number += 1
          },
          tambahJawaban() {
            if (this.banyakJawaban < 5) {
              console.log(this.Quiz)
              for (var i = 0; i < this.Quiz.length; i++) {
                this.Quiz[i].quizAnswer.push({
                  quiz_answer_content: '', 
                  quiz_answer_key: this.banyakJawaban+1
                })
              }
              this.banyakJawaban += 1
            }
          },
          kurangiJawaban() {
            if (this.banyakJawaban > 1) {
              for (var i = 0; i < this.Quiz.length; i++) {
                console.log(this.banyakJawaban)
                this.Quiz[i].quizAnswer.splice(this.banyakJawaban-1, 1)
              }
              this.banyakJawaban -= 1
            }
          },
          kurangiSoal(index) {
            this.Quiz.splice(index, 1)
            this.banyakSoal2 -=1
          },
          render(data) {
            this.banyakSoal2 = this.banyakSoal - 1             
        }
      }
  });
</script>