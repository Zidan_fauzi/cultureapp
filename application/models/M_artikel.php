<?php

class M_artikel extends CI_Model{
    private $_table = "tb_berita";

    public function getAll()
    {
        $this->db->select("
            article.article_id,
            article.article_title,
            article.article_image,
            article.article_description,
            article.created_at,
            page.page_number,
            quiz.quiz_id
        ")
        ->join('quiz', "quiz.article_id = article.article_id", 'LEFT OUTER')
        ->join('page', "page.article_id = article.article_id", 'LEFT OUTER');

        $this->db->group_by("article.article_id");
        $this->db->order_by("article.article_id DESC");

        $query = $this->db->get("article")->result_array();
        return $query;
    }

    public function getAllSubab()
    {
        $this->db->select("
            subab.subab_id,
            subab.subab_title,
            subab.subab_image,
            subab.category_id,
            subab.subab_description,
            category.category_name,
            subab.created_at
        ")->join('category', "category.category_id = subab.category_id", 'LEFT OUTER');
        $this->db->order_by("subab.subab_id DESC");

        $query = $this->db->get("subab")->result_array();
        return $query;
    }
    
    public function getById($id)
    {
        $this->db->select("
            article.article_id,
            article.article_title,
            article.article_image,
            article.article_description,
            article.subab_id,
            article.created_at,
            page.page_number,
            page.page_id,
            quiz.quiz_total_answer
        ")
        ->join('page', "page.article_id = article.article_id", 'LEFT OUTER')
        ->join('quiz', "quiz.article_id = article.article_id", 'LEFT OUTER')
        ->order_by("article.article_id ASC")
        ->group_by("article.article_id");

        $query = $this->db->get_where("article", ["article.article_id" => $id])->row_array();
        return $query;
    }

    public function getAllBySubab($id)
    {
        $this->db->select("
            article.article_id,
            article.article_title,
            article.article_image,
            article.article_description,
            article.created_at,
            page.page_number,
            page.page_id,
            quiz.quiz_total_answer
        ")
        ->join('page', "page.article_id = article.article_id", 'LEFT OUTER')
        ->join('quiz', "quiz.article_id = article.article_id", 'LEFT OUTER')
        ->order_by("article.article_id ASC")
        ->group_by("article.article_id");

        $query = $this->db->get_where("article", ["article.subab_id" => $id])->result_array();
        return $query;
    }

    public function getAllSubabById($id)
    {
        $this->db->select("
            subab.subab_id,
            subab.subab_title,
            subab.subab_image,
            subab.category_id,
            subab.subab_description,
            subab.created_at,
            category.category_name
        ")->join('category', "category.category_id = subab.category_id", 'LEFT OUTER');
        $this->db->order_by("subab.subab_id DESC");

        $query = $this->db->get_where("subab", ["subab.subab_id" => $id])->row_array();
        return $query;
    }

    public function getPageDetail($id)
    {
        $this->db->select("*")
        ->order_by("page_detail.page_detail_order ASC");

        $query = $this->db->get_where("page_detail", ["page_detail.page_id" => $id])->result_array();
        $x = count($query);
        $responer = array();
        for ($i=0; $i < $x ; $i++) { 
           $responer[$i] = array(
            'number' => $i,
            'type' => $query[$i]["page_detail_type"],
            'content' => $query[$i]["page_detail_content"]
           );
        }
        return $responer;
    }

    public function uploadArticle($data_article) 
    {
        return $this->db->insert('article', $data_article);
    }
    public function uploadSubab($data_article) 
    {
        return $this->db->insert('subab', $data_article);
    }
    public function uploadPage($data_page) 
    {
        return $this->db->insert('page', $data_page);
    }

    public function updateArticle($id, $data_article) 
    {
        $this->db->where('article.article_id = '.$id);
        return $this->db->update('article', $data_article);
    }
    public function updateSubab($data_subab, $id) 
    {
        $this->db->where('subab.subab_id = '.$id);
        return $this->db->update('subab', $data_subab);
    }
    public function updatePage($id, $data_page) 
    {   
        $this->db->where('page.page_id = '.$id);
        return $this->db->update('page', $data_page);
    }

    public function uploadPageDetail($data_page_detail) 
    {
        return $this->db->insert('page_detail', $data_page_detail);
    } 

    public function deletePageDetail($id) 
    {
        $this->db->where('page_detail.page_id = '.$id);
        return $this->db->delete('page_detail');
    }  

    public function deletePage($id) 
    {
        $this->db->delete('page', array('page.page_id' => $id));
    }    
    
    public function delete($id)
    {
        $this->db->delete('article', array('article.article_id' => $id));
    }

    public function deleteSubab($id)
    {
        $this->db->delete('subab', array('subab_id' => $id));
    }
}		
