<?php

class M_user extends CI_Model{
    private $_table = "tb_berita";

    public function getAll()
    {
        $this->db->select("
            tb_admin.id_admin,
            tb_admin.gambar,
            tb_admin.nama,
            tb_admin.level,
            tb_admin.email,
            tb_admin.created_at,
            tb_admin.updated_at,
        ")->where('tb_admin.is_delete = 0 AND tb_admin.level = 1');

        $query = $this->db->get("tb_admin")->result_array();
        return $query;
    }
    
    public function getById($id)
    {
        $this->db->select("
            tb_admin.id_admin,
            tb_admin.gambar,
            tb_admin.nama,
            tb_admin.level,
            tb_admin.email,
            tb_admin.created_at,
            tb_admin.updated_at,
        ")->where('tb_admin.is_delete = 0 AND tb_admin.level = 1');

        $query = $this->db->get_where("tb_admin", ["tb_admin.id_admin" => $id])->row_array();
        return $query;       
    }
    
    public function delete($id)
    {
        $this->db->delete('tb_admin', array('id_admin' => $id));
    }
}		
