<?php

class M_login extends CI_Model{
    private $_table3 = "tb_admin";
    private $_table2 = "tb_art";
    
    public $id_admin;
    public $email;
    public $nama;
    public $password;
    public $level;
    
    function logged_id(){
        return $this->session->userdata('level');
    }

    function check_login($_table2, $field2, $field5){
        $this->db->select('*');
        $this->db->from($_table2);
        $this->db->where($field2);
        $this->db->where($field5);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function logged_in($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update($this->_table3, $data, array('id_admin' => $id));
    }

    function logout_at($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'cookie' => '',
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update('tb_admin', $data, array('id_admin' => $id));
    }
    
    function register($data){      
        $this->db->insert('tb_admin', $data);
    }

    public function checkUser($email)
    {
        $this->db->select("*")->where("tb_admin.email = '$email'");
        $this->db->order_by("tb_admin.id_admin ASC");
        $query = $this->db->get("tb_admin")->result();
        return $query;
    }

    function request($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'id_admin' => $id,
            'status' => '2',
            'created_at' => date("y-m-d H:i:s"),
            'updated_at' => date("y-m-d H:i:s"),
            'is_delete' => '0'
        );
        $this->db->insert('user_request', $data);
    }

    function get_by_cookie($cookie){
        $this->db->select('*');
        $this->db->from('tb_admin');
        $this->db->where('cookie', $cookie);
        return $this->db->get();
    }

    function update($update_data, $id){
        $this->db->where('tb_admin.id_admin = '.$id);
        return $this->db->update('tb_admin', $update_data);
    }
  }     