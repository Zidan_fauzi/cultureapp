<?php

class M_quiz extends CI_Model{
    private $_table = "tb_berita";    

    public function getQuiz($id)
    {
        $this->db->select("
            quiz.quiz_id,
            quiz.quiz_content,
            quiz.article_id,
            quiz.quiz_total_answer,
            article.article_title,
            quiz.created_at,
            quiz_answer_key.quiz_answer_key
        ")
        ->join('quiz_answer_key', "quiz_answer_key.quiz_id = quiz.quiz_id", 'LEFT OUTER')
        ->join('article', "article.article_id = quiz.article_id", 'LEFT OUTER')
        ->order_by("quiz.created_at ASC");

        $query = $this->db->get_where("quiz", ["quiz.article_id" => $id])->result_array();
        return $query;
    }

    public function getQuizAnswer($id)
    {
        $this->db->select("
            quiz_answer.quiz_answer_id,
            quiz_answer.quiz_answer_content,
            quiz_answer.quiz_answer_key
        ")
        ->order_by("quiz_answer.quiz_answer_id ASC");

        $query = $this->db->get_where("quiz_answer", ["quiz_answer.quiz_id" => $id])->result_array();
        return $query;
    }

    public function getQuizById($id)
    {
        $this->db->select("
            quiz.quiz_id,
            quiz.quiz_content,
            quiz.article_id,
            article.article_title,
            quiz.created_at,
            quiz_answer_key.quiz_answer_key
        ")
        ->join('quiz_answer_key', "quiz_answer_key.quiz_id = quiz.quiz_id", 'LEFT OUTER')
        ->join('article', "article.article_id = quiz.article_id", 'LEFT OUTER')
        ->order_by("quiz.created_at ASC");

        $query = $this->db->get_where("quiz", ["quiz.article_id" => $id])->result_array();
        return $query;
    }

    public function getQuizByArticle($id)
    {
        $this->db->select("
            quiz.quiz_id,
            quiz.quiz_content,
            quiz.article_id,
            quiz.quiz_total_answer,
            article.article_title,
            quiz.created_at,
            quiz_answer_key.quiz_answer_key
        ")
        ->join('quiz_answer_key', "quiz_answer_key.quiz_id = quiz.quiz_id", 'LEFT OUTER')
        ->join('article', "article.article_id = quiz.article_id", 'LEFT OUTER')
        ->order_by("quiz.created_at ASC");

        $query = $this->db->get_where("quiz", ["quiz.article_id" => $id])->row_array();
        return $query;
    }

    public function uploadQuiz($data) 
    {
        return $this->db->insert('quiz', $data);
    }
    
    public function uploadQuizAnswer($data) 
    {
        return $this->db->insert('quiz_answer', $data);
    }

    public function uploadQuizAnswerKey($data) 
    {
        return $this->db->insert('quiz_answer_key', $data);
    }

    public function updateQuiz($data) 
    {
        return $this->db->insert('quiz', $data);
    }
    
    public function deleteQuiz($id)
    {
        $this->db->delete('quiz', array('article_id' => $id));
    }

    public function deleteAnswer($id)
    {
        $this->db->delete('quiz_answer', array('quiz_id' => $id));
    }

    public function deleteAnswerKey($id)
    {
        $this->db->delete('quiz_answer_key', array('quiz_id' => $id));
    }
}		
