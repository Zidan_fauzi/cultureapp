-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 07:27 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_portal_berita`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` bigint(4) NOT NULL,
  `article_title` varchar(150) NOT NULL,
  `article_image` text NOT NULL,
  `article_description` text NOT NULL,
  `created_at` date NOT NULL,
  `subab_id` bigint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`article_id`, `article_title`, `article_image`, `article_description`, `created_at`, `subab_id`) VALUES
(2, 'Subab 1', 'file/file_1587849766.jpg', 'Subab 1', '2020-04-26', 16);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` bigint(4) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(1, 'Sosial'),
(2, 'Budaya');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` bigint(4) NOT NULL,
  `user_id` bigint(4) NOT NULL,
  `article_id` bigint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` bigint(4) NOT NULL,
  `article_id` bigint(4) NOT NULL,
  `page_number` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `article_id`, `page_number`) VALUES
(3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_detail`
--

CREATE TABLE `page_detail` (
  `page_detail_id` bigint(4) NOT NULL,
  `page_detail_type` enum('pengertian','subab','description') NOT NULL,
  `page_detail_content` text NOT NULL,
  `page_detail_order` int(4) NOT NULL,
  `page_id` bigint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_detail`
--

INSERT INTO `page_detail` (`page_detail_id`, `page_detail_type`, `page_detail_content`, `page_detail_order`, `page_id`) VALUES
(9, 'description', '<p>Subab 1</p>\r\n', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `quiz_id` bigint(4) NOT NULL,
  `quiz_content` text NOT NULL,
  `quiz_total_answer` int(2) NOT NULL,
  `article_id` bigint(4) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `quiz_content`, `quiz_total_answer`, `article_id`, `created_at`) VALUES
(1, 'asjgvxgjavscvjsac', 3, 2, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `quiz_answer_id` bigint(4) NOT NULL,
  `quiz_answer_content` text NOT NULL,
  `quiz_id` bigint(4) NOT NULL,
  `quiz_answer_key` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`quiz_answer_id`, `quiz_answer_content`, `quiz_id`, `quiz_answer_key`) VALUES
(1, 'asjgvxgjavscvjsac', 1, 1),
(2, 'asjgvxgjavscvjsac', 1, 2),
(3, 'asjgvxgjavscvjsac', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer_key`
--

CREATE TABLE `quiz_answer_key` (
  `quiz_answer_key_id` bigint(4) NOT NULL,
  `quiz_id` bigint(8) NOT NULL,
  `quiz_answer_key` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_answer_key`
--

INSERT INTO `quiz_answer_key` (`quiz_answer_key_id`, `quiz_id`, `quiz_answer_key`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subab`
--

CREATE TABLE `subab` (
  `subab_id` bigint(4) NOT NULL,
  `subab_title` varchar(255) NOT NULL,
  `subab_image` text NOT NULL,
  `category_id` bigint(4) NOT NULL,
  `subab_description` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subab`
--

INSERT INTO `subab` (`subab_id`, `subab_title`, `subab_image`, `category_id`, `subab_description`, `created_at`) VALUES
(16, 'Article 1', 'file/Subab_image_1587849788.jpg', 2, 'Article 1', '2020-04-26 04:23:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` bigint(4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL,
  `cookie` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `email`, `nama`, `password`, `gambar`, `level`, `cookie`, `updated_at`, `created_at`, `is_delete`) VALUES
(1, 'admin@gmail.com', 'admin', '942ae13e7ef81210c3675d5a3358eca0', 'gambar_profile/file_1578976439.png', '0', 'xYOBAlrpkj3nwSYufZR0cqMEmRtNJEUpTVytxwJHobGFWav5a0Q6mhjC6XgPzUQs', '2020-04-27 12:09:10', '2020-01-12 00:00:00', 0),
(5, 'zfauzi69@gmail.com', 'Mohammad Zidan Fauzi', '89c03dee3d5e34afe83600f3ddd713d5', 'file/ImageProfile_1587347068.png', '1', '', '2020-04-20 08:44:28', '2020-04-20 08:44:28', 0),
(9, 'Anton123@yahoo.com', 'Anton Pramana', '2c08d6dc3fcd7c7d06a089af2c12ef36', '', '1', '', '2020-04-22 08:48:58', '2020-04-22 08:48:58', 0),
(10, 'email1@gmail.com', 'username1', '47b03610e3d77651a5b3b3f1eff32f7f', '', '1', '', '2020-04-22 13:23:38', '2020-04-22 13:23:38', 0),
(11, 'email2@gmail.com', 'username2', '6249b9f690bc7327bc85b094cc65bb1b', '', '1', '', '2020-04-22 13:24:07', '2020-04-22 13:24:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_quiz`
--

CREATE TABLE `user_quiz` (
  `id_user_quiz` int(11) NOT NULL,
  `id_admin` bigint(4) NOT NULL,
  `quiz_id` bigint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_quiz`
--

INSERT INTO `user_quiz` (`id_user_quiz`, `id_admin`, `quiz_id`) VALUES
(1, 5, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `page_detail`
--
ALTER TABLE `page_detail`
  ADD PRIMARY KEY (`page_detail_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quiz_id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`quiz_answer_id`);

--
-- Indexes for table `quiz_answer_key`
--
ALTER TABLE `quiz_answer_key`
  ADD PRIMARY KEY (`quiz_answer_key_id`);

--
-- Indexes for table `subab`
--
ALTER TABLE `subab`
  ADD PRIMARY KEY (`subab_id`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `user_quiz`
--
ALTER TABLE `user_quiz`
  ADD PRIMARY KEY (`id_user_quiz`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` bigint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `page_detail`
--
ALTER TABLE `page_detail`
  MODIFY `page_detail_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quiz_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `quiz_answer_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz_answer_key`
--
ALTER TABLE `quiz_answer_key`
  MODIFY `quiz_answer_key_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subab`
--
ALTER TABLE `subab`
  MODIFY `subab_id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_quiz`
--
ALTER TABLE `user_quiz`
  MODIFY `id_user_quiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
